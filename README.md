# Search With Wolfram|alpha

This is a Python project will get search results in Wolfram|alpha

## Installation

Make use of the requirements.txt in the folder to install all necessary library use
### Windows base Operating System

```bash
pip install -r requirements.txt
```

### Macosx and Linux based Operating System

```bash
pip3 install -r requirements.txt
```

## Usage

Just simply double click the .py file or use

### Macosx or Linux Based Operating Systems

```bash
python3 Main.py
```

### Windows Based Operating Systems

```bash
python Main.py
```

## Contributing
Pull requests are welcome. For Minor and Major changes, please open an issue first to discuss what you would like to change.

Please make sure to update test as appropriate.

## License

[Apache](./LICENSE.txt)