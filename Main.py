from tkinter import *
from tkinter import messagebox
import wolframalpha
client = wolframalpha.Client('PK528G-2UVT7YTQGE')
root = Tk()
root.title("WolframalphaSearch BETA1")
root.geometry("250x75")
def findalpha():
	user_search = search_entry.get()
	res = client.query(user_search)
	messagebox.showinfo('alpha', next(res.results).text)
entry_label = Label(text = "Search Here")
entry_label.grid(row = 1, column = 1)

search_entry = Entry()
search_entry.grid(row = 1, column = 2)

search_button = Button(text = "Search", command = findalpha)
search_button.grid(row = 2, column = 2)
root.mainloop()